# Markdown Redactor

Конвертируйте markdown в HTML. Можно использовать готовые библиотеки.


## User Stories

- Пользователь может ввести markdown в  `textarea`
- Пользователь может увидеть распарсеный `HTML` в другом контейнере.
- Пользователь может видеть, что распарсеный `HTML` автоматически обновляется при изменении `textarea`.
- При закрытии окна браузера markdown будет сохранен в `IndexedDB`, и когда пользователь вернется, данные будут извлечены и отображены.
- Пользователь может нажать кнопку, и распарсеный HTML будет сохранен в буфере обмена.

## Полезные материалы

-   [Markdown Guide](https://www.markdownguide.org/basic-syntax/)
-   [How to Copy to Clipboard](https://www.w3schools.com/howto/howto_js_copy_clipboard.asp)

## Примеры
-   [Markdown Live Preview](https://markdownlivepreview.com/)
-   ![example gif](./ezgif.com-gif-maker.gif)

## Дополнительно

Залейте исходники на GitHub или Gitlab. Используйте Vue 2
